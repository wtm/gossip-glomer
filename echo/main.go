package main

import (
	"encoding/json"
	"log"

	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
)

func main() {
    n := maelstrom.NewNode()
    n.Handle("echo", func(msg maelstrom.Message) error {
        var msgBody map[string]any
        if err := json.Unmarshal(msg.Body, &msgBody); err != nil {
            return err
        }
        reply := make(map[string]string)
        reply["type"] = "echo_ok"
        reply["echo"] = msgBody["echo"].(string)
        return n.Reply(msg, reply)
    })
    if err := n.Run(); err != nil {
        log.Fatal(err)
    }
}
