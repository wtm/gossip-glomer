package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"
	"sort"
	"sync"
	"time"

	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
	"golang.org/x/exp/maps"
	"golang.org/x/exp/slices"
)

type GossipMsg struct {
	// Must be "gossip"
	Ty string `json:"type"`

	Values []float64 `json:"values"`
}

type AcknowledgeMsg struct {
	// Must be "acknowledge"
	Ty string `json:"type"`
}

type NodeState struct {
	lock           sync.Mutex
	knowledge      map[float64]bool
	cooldown_until *time.Time
}

func SetDiff[K comparable, V any](a map[K]V, b map[K]V) (res []K) {
	res = make([]K, 0)
	for k := range a {
		_, exist_in_b := b[k]
		if !exist_in_b {
			res = append(res, k)
		}
	}
	return
}

func main() {
	n := maelstrom.NewNode()
	values := make(map[float64]bool)
	values_lock := sync.RWMutex{}
	var node_states map[string]*NodeState
	var myNeigh []string

	n.Handle("topology", func(msg maelstrom.Message) error {
		myId := n.ID()
		var mBody map[string]any
		if err := json.Unmarshal(msg.Body, &mBody); err != nil {
			return err
		}
		topology := mBody["topology"].(map[string]any)
		neighSet := make(map[string]bool, 0)
		for id, _neighs := range topology {
			neighs := _neighs.([]any)
			if id == myId {
				for _, neigh := range neighs {
					neighSet[neigh.(string)] = true
				}
			} else if slices.ContainsFunc(neighs, func(elem any) bool { return elem.(string) == myId }) {
				neighSet[id] = true
			}
		}
		myNeigh = maps.Keys(neighSet)
		node_states = make(map[string]*NodeState)
		for _, neigh := range myNeigh {
			node_states[neigh] = &NodeState{
				knowledge: make(map[float64]bool),
			}
		}
		reply := map[string]string{
			"type": "topology_ok",
		}
		fmt.Fprintln(os.Stderr, myNeigh)
		return n.Reply(msg, reply)
	})

	n.Handle("broadcast", func(msg maelstrom.Message) error {
		var mBody map[string]any
		if err := json.Unmarshal(msg.Body, &mBody); err != nil {
			return err
		}
		values_lock.Lock()
		values[mBody["message"].(float64)] = true
		values_lock.Unlock()
		reply := map[string]string{
			"type": "broadcast_ok",
		}
		return n.Reply(msg, reply)
	})

	n.Handle("read", func(msg maelstrom.Message) error {
		values_lock.RLock()
		v := maps.Keys(values)
		values_lock.RUnlock()
		sort.Float64s(v)
		reply := map[string]any{
			"type":     "read_ok",
			"messages": v,
		}
		return n.Reply(msg, reply)
	})

	n.Handle("gossip", func(msg maelstrom.Message) error {
		var body GossipMsg
		if err := json.Unmarshal(msg.Body, &body); err != nil {
			return err
		}
		ns := node_states[msg.Src]
		ns.lock.Lock()
		values_lock.Lock()
		for _, v := range body.Values {
			values[v] = true
			ns.knowledge[v] = true
		}
		ns.lock.Unlock()
		values_lock.Unlock()
		reply := AcknowledgeMsg{
			Ty: "acknowledge",
		}
		return n.Reply(msg, reply)
	})

	go func() {
		for {
			values_lock.RLock()
			var next_call_on *time.Time

			if len(values) == 0 {
				values_lock.RUnlock()
				time.Sleep(time.Millisecond * 100)
				continue
			}

			values_lock.RUnlock()

			for dst, ns := range node_states {
				err, r := trySendGossip(ns, n, dst, values, &values_lock)
				if next_call_on == nil || next_call_on.After(r) {
					next_call_on = &r
				}
				if err != nil {
					log.Fatal(err)
					return
				}
			}

			now := time.Now()
			if next_call_on != nil && next_call_on.After(now) {
				time.Sleep(next_call_on.Sub(now))
			} else {
				time.Sleep(time.Second)
			}
		}
	}()

	if err := n.Run(); err != nil {
		log.Fatal(err)
	}
}

// Will lock `ns`
//
// Return next call time
func trySendGossip(ns *NodeState, n *maelstrom.Node, dst string, values map[float64]bool, values_lock *sync.RWMutex) (err error, next_call_on time.Time) {
	ns.lock.Lock()
	defer ns.lock.Unlock()
	now := time.Now()
	if ns.cooldown_until != nil && now.Before(*ns.cooldown_until) {
		next_call_on = *ns.cooldown_until
		return
	} else {
		values_lock.RLock()
		values_to_send := SetDiff(values, ns.knowledge)
		values_lock.RUnlock()
		sort.Float64s(values_to_send)
		if len(values_to_send) > 0 {
			err = n.RPC(dst, GossipMsg{
				Ty:     "gossip",
				Values: values_to_send,
			}, func(msg maelstrom.Message) (err error) {
				var body AcknowledgeMsg
				if err = json.Unmarshal(msg.Body, &body); err != nil {
					return
				}
				if body.Ty != "acknowledge" {
					err = fmt.Errorf("Unexpected reply %s", body.Ty)
					return
				}
				ns.lock.Lock()
				defer ns.lock.Unlock()
				for _, v := range values_to_send {
					ns.knowledge[v] = true
				}
				return
			})
		}
		delay_ms := 85 + rand.Int31n(30)
		_n := time.Now().Add(time.Millisecond * time.Duration(delay_ms))
		next_call_on = _n
		ns.cooldown_until = &_n
		return
	}
}
