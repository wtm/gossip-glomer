module go.mww.moe/gossip-glomer/unique-id

go 1.20

require github.com/jepsen-io/maelstrom/demo/go v0.0.0-20230321201811-151ad3cff117

require github.com/oklog/ulid/v2 v2.1.0 // indirect
