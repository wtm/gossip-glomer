package main

import (
	"log"

	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
	ulid "github.com/oklog/ulid/v2"
)

func main() {
    n := maelstrom.NewNode()
    n.Handle("generate", func(msg maelstrom.Message) error {
        reply := make(map[string]string)
        reply["type"] = "generate_ok"
        reply["id"] = ulid.Make().String()
        return n.Reply(msg, reply)
    })
    if err := n.Run(); err != nil {
        log.Fatal(err)
    }
}
